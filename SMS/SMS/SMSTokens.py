from SMS import SMSParser

def pop(stack):
    print (stack[0])
    SMSParser.update_stack(stack[1:])

def pop_chr(stack):
    s = stack[0]
    if type(s) == int:
        print (chr(stack[0]))
    elif type(s) == str:
        try:
            print (ord(s))
        except:
            try:
                print (chr(int(s)))
            except:
                print (s)
    else:
        print(s)
    SMSParser.update_stack(stack[1:])

def repr_print(stack):
    print (repr(stack[0]))
    SMSParser.update_stack(stack[1:])

def remove(stack):
    SMSParser.update_stack(stack[1:])

def swap(stack):
    s0 = stack[0]
    s1 = stack[1]
    SMSParser.update_stack([s1, s0] + stack[2:])

def string(stack):
    SMSParser.update_stack([str(stack[0])] + stack[1:])

def duplicate(stack):
    SMSParser.update_stack([stack[0]] + stack)

def dump(stack):
    print (stack[::-1])

def integer(stack):
    try:
        SMSParser.update_stack([int(stack[0])] + stack[1:])
    except:
        SMSParser.update_stack([ord(stack[0])] + stack[1:])

def divide(stack):
    s0 = stack[0]
    s1 = stack[1]
    SMSParser.update_stack([s0 // s1] + stack[2:])

def multiply(stack):
    s0 = stack[0]
    s1 = stack[1]
    SMSParser.update_stack([s0 * s1] + stack[2:])

def subtract(stack):
    s0 = stack[0]
    s1 = stack[1]
    SMSParser.update_stack([s0 - s1] + stack[2:])

def add(stack):
    s0 = stack[0]
    s1 = stack[1]
    SMSParser.update_stack([s0 + s1] + stack[2:])

def find(stack):
    SMSParser.update_stack([stack[stack[0]]] + stack[1:stack[0]] + stack[stack[0]:])

def assign(stack):
    tokens[stack[0]] = lambda s: stack[1]
    SMSParser.update_stack(stack[2:])

def inp(stack):
    SMSParser.update_stack([input()] + stack)

tokens = {'!': pop,
          '.': pop_chr,
          ';': remove,
          '\\': swap,
          '\'': string,
          'r': repr_print,
          'd': duplicate,
          'u': dump,
          'f': find,
          '+': add,
          '-': subtract,
          '*': multiply,
          '/': divide,
          ':': assign,
          'i': integer,
          '~': inp,
          '0': lambda stack: SMSParser.update_stack([0] + stack),
          '1': lambda stack: SMSParser.update_stack([1] + stack),
          '2': lambda stack: SMSParser.update_stack([2] + stack),
          '3': lambda stack: SMSParser.update_stack([3] + stack),
          '4': lambda stack: SMSParser.update_stack([4] + stack),
          '5': lambda stack: SMSParser.update_stack([5] + stack),
          '6': lambda stack: SMSParser.update_stack([6] + stack),
          '7': lambda stack: SMSParser.update_stack([7] + stack),
          '8': lambda stack: SMSParser.update_stack([8] + stack),
          '9': lambda stack: SMSParser.update_stack([9] + stack),
          'A': lambda stack: SMSParser.update_stack([10] + stack),
          'B': lambda stack: SMSParser.update_stack([11] + stack),
          'C': lambda stack: SMSParser.update_stack([12] + stack),
          'D': lambda stack: SMSParser.update_stack([13] + stack),
          'E': lambda stack: SMSParser.update_stack([14] + stack),
          'F': lambda stack: SMSParser.update_stack([15] + stack),
          'N': lambda stack: SMSParser.update_stack([-1] + stack)}

dummytokens = "\n\t "
for i in dummytokens:
    tokens[i] = lambda stack:0