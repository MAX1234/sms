from SMS import SMSParser
import sys

if __name__ == '__main__':
    file = sys.argv[1]
    with open(file) as f:
        SMSParser.parse(f)