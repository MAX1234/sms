from SMS import SMSTokens, SMSError, SMSMultiDict
from SMS.SMSMultiDict import Not
import re

triggers = SMSMultiDict.SMSMultiDict()

stack = []

PATTERN = '\[ *( *(\d+|\".+?\"|Not *\( *\d+ *\)), *)*( *(\d+|\".+?\"|Not *\( *\d+ *\)),? *)? *]'

def update_stack(new_stack):
    global stack
    stack = new_stack
    if tuple(new_stack) in triggers:
        eval_code(triggers[tuple(new_stack)])

def update_triggers(trigger, code):
    if trigger in triggers.keys():
        triggers[trigger] = triggers[trigger] + [code]
    else:
        triggers[trigger] = [code]

def parse(file):
    for line in file.readlines():
        match = tuple(eval(re.match(PATTERN, line).group(0))) # TODO: don't eval
        rol = re.sub(PATTERN + ':', '', line) # Rest Of Line
        update_triggers(match, rol)

    update_stack([])

def eval_code(codes):
    for code in list(codes):
        for line in code:
            for char in line:
                if char in SMSTokens.tokens.keys():
                    try:
                        SMSTokens.tokens[char](stack)
                    except IndexError:
                        raise SMSError.SMSError("Stack has invalid depth: %s" % len(stack))
                else:
                    raise SMSError.SMSError("Token not found: '%s'" % char)