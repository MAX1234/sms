class SMSMultiDict():
    d = []
    def __init__(self): pass
    def __getitem__(self, item):
        for i in self.d:
            if i[0] == item:
                yield i[1]
    def __setitem__(self, key, value):
        self.d.append((key, value))

    def keys(self):
        return list(map(lambda x:x[0], self.d))

    def values(self):
        return list(map(lambda x:x[1], self.d))

    def __contains__(self, item):
        for i in self.d:
            if i[0] == item:
                return True

class Not():
    a = 0
    def __init__(self, n):
        self.a = n

    def __eq__(self, other):
        return other != self.a